WorkFlow Make Human to SDF: 

    Design Human in MakeHuman 

    Add bone model 

    Export as Collada file 

    Import collada in blender (using blender import functions) 

    RD: Segments -> import native Blender segment -> proceed recursively 

    Geometries -> Generate Geometry -> Generate geometry for all segments 

    Detach human_model and all meshes that are not needed (big cylinders) 

    Generate collision meshes -> Generate All Collision meshes 

    Detach wrong ones, the big disturbing ones

    Export sdf 

    Delete first joint (the one without parent) 

    Set model pose in sdf
    <model>
    	<pose>0 0 1 1.58 0 0</pose> 
    ...
    </model>

    Rescale: add <scale>0.1 0.1 0.1</scale> to every mesh – not to collision meshes
