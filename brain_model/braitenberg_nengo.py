import nengo

circuit = nengo.Network()
with circuit:
    # Now we create an Ensembles of 1000 neurons.  These 1000 neurons are
    # configured to form a distributed representation of three values for
    # sensation, and two values for actuation (i.e.
    # each neuron gets an input current that is some random combination
    # of those three values). The actors population simply redirects the
    # neurally encoded value to the connected motors of the husky robot.
    sensors = nengo.Ensemble(n_neurons=1000, dimensions=3)
    actors = nengo.Ensemble(n_neurons=2, dimensions=2, neuron_type=nengo.Direct())

    # Now we tell Nengo what computation we want the neurons to
    # approximate.  Nengo will use this to find connection weights
    # between 'sensors' (the perception ensemble) and 'actors' (the output ensemble)
    # that will best approximate the function we give it.
    #
    # It should be noted that we can do anything we want
    # here in this function.  We could do some complex algorithm
    # involving trig functions, multiple branching if statements,
    # or whatever.  Nengo will attempt to approximate that function
    # using the neurons.
    #
    # The synapse=0.1 parameter indicates that the connections
    # defined here have a synapse that is an exponential low-pass
    # filter with a time constant of 100ms.
    def control(x):
        rl, rr, gb = x
        forward = min(1.0, max(-0.01, (min(rl, rr)) * 100000))
        turn = max(min((rl - rr) * 100, 1), -1)
        if max(rl, rr) < 0.1:
            forward = -0.001
            turn = 0.5
        return forward, turn

    nengo.Connection(sensors, actors, function=control, synapse=0.1)
