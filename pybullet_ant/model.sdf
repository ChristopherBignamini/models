<?xml version="1.0"?>

<!--License-->
<!--pybullet-->

<!--The files in this repository are licensed under the zlib license, except for the files under 'Extras' and examples/ThirdPartyLibs.-->

<!--Bullet Continuous Collision Detection and Physics Library-->
<!--http://bulletphysics.org-->

<!--This software is provided 'as-is', without any express or implied warranty.-->
<!--In no event will the authors be held liable for any damages arising from the use of this software.-->
<!--Permission is granted to anyone to use this software for any purpose,-->
<!--including commercial applications, and to alter it and redistribute it freely,-->
<!--subject to the following restrictions:-->

<!--1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.-->
<!--2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.-->
<!--3. This notice may not be removed or altered from any source distribution.-->

<!--Roboschool-->

<!--The MIT License-->

<!--Copyright (c) 2017 OpenAI (http://openai.com)-->

<!--Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:-->

<!--The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.-->

<!--THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.-->
<!--Mujoco models-->

<!--This work is derived from MuJuCo models (http://www.mujoco.org/forum/index.php?resources/) used under the following license:-->

<!--This file is part of MuJoCo.-->
<!--Copyright 2009-2015 Roboti LLC.-->
<!--Mujoco		:: Advanced physics simulation engine-->
<!--Source		: www.roboti.us-->
<!--Version		: 1.31-->
<!--Released 	: 23Apr16-->
<!--Author		:: Vikash Kumar-->
<!--Contacts 	: kumar@roboti.us-->

<sdf version="1.6">
  <model name="pybullet_ant">
    <pose>0 0 0.75 0 0 0</pose>
    <!-- Torso of the Ant-->
    <link name="torso">
      <frame name="torso_frame"/>
      <visual name="torso_visual">
        <geometry>
          <sphere>
            <radius>0.25</radius>
          </sphere>
        </geometry>
      </visual>
      <collision name="torso_collision">
        <geometry>
          <sphere>
            <radius>0.25</radius>
          </sphere>
        </geometry>
      </collision>
    </link>

    <!-- The PyBullet Ant uses capsules as base geometry objects for the legs. Since they are not default objects in -->
    <!-- SDF the capsules are custom defined. The Ant uses per leg two small capsules and one large capsule. They -->
    <!-- defined in the subfolders small_capsule and large_capsule. Note that they are specified for the front right -->
    <!-- leg, so they need to be rotated accordingly for every other leg.-->

    <!-- < Front left part > -->

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">0 0 0 0 0 0</pose>
      <name>front_left_leg</name>
    </include>

    <joint name="torso_leg_joint_1" type="fixed">
      <parent>torso</parent>
      <child>front_left_leg::bottom</child>
    </joint>

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">0.2 0.2 0 0 0 0</pose>
      <name>aux_1</name>
    </include>

    <joint name="hip_1" type="revolute">
      <parent>front_left_leg::top</parent>
      <child>aux_1::bottom</child>
      <axis>
        <xyz>0 0 1</xyz>
        <limit>
          <!-- -40 to 40 degrees-->
          <lower>-0.69813170079</lower>
          <upper>0.69813170079</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <include>
      <uri>model://pybullet_ant/large_capsule</uri>
      <pose frame="torso_frame">0.4 0.4 0 0 0 0</pose>
      <name>front_left_foot</name>
    </include>

<!-- In PyBullet the joints do not rotate, meaning their direction is the same as the global coordinate system. In -->
<!-- Gazebo this is not the case, therefore we have to specify the right rotation axis for the joints. So these axis -->
<!-- are different to the original MJCF XML for the Ant. Note that this is the case for every "ankle_x" joint.-->

    <joint name="ankle_1" type="revolute">
      <parent>aux_1::top</parent>
      <child>front_left_foot::bottom</child>
      <axis>
        <xyz>-1 1 0</xyz>
        <limit>
          <!-- 30 to 100 degrees-->
          <lower>0.52359877559</lower>
          <upper>1.74532925199</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <!-- < Front right part > -->

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">0 0 0 0 0 1.57079632679</pose>
      <name>front_right_leg</name>
    </include>

    <joint name="torso_leg_joint_2" type="fixed">
      <parent>torso</parent>
      <child>front_right_leg::bottom</child>
    </joint>

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">-0.2 0.2 0 0 0 1.57079632679</pose>
      <name>aux_2</name>
    </include>

    <joint name="hip_2" type="revolute">
      <parent>front_right_leg::top</parent>
      <child>aux_2::bottom</child>
      <axis>
        <xyz>0 0 1</xyz>
        <limit>
          <!-- -40 to 40 degrees-->
          <lower>-0.69813170079</lower>
          <upper>0.69813170079</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <include>
      <uri>model://pybullet_ant/large_capsule</uri>
      <pose frame="torso_frame">-0.4 0.4 0 0 0 1.57079632679</pose>
      <name>front_right_foot</name>
    </include>

    <joint name="ankle_2" type="revolute">
      <parent>aux_2::top</parent>
      <child>front_right_foot::bottom</child>
      <axis>
        <xyz>1 -1 0</xyz>
        <limit>
          <!-- -100 to -30 degrees-->
          <lower>-1.74532925199</lower>
          <upper>-0.52359877559</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <!-- < Back left part > -->

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">0 0 0 0 0 3.14159265359</pose>
      <name>back_left_leg</name>
    </include>

    <joint name="torso_leg_joint_3" type="fixed">
      <parent>torso</parent>
      <child>back_left_leg::bottom</child>
    </joint>

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">-0.2 -0.2 0 0 0 3.14159265359</pose>
      <name>aux_3</name>
    </include>

    <joint name="hip_3" type="revolute">
      <parent>back_left_leg::top</parent>
      <child>aux_3::bottom</child>
      <axis>
        <xyz>0 0 1</xyz>
        <limit>
          <!-- -40 to 40 degrees-->
          <lower>-0.69813170079</lower>
          <upper>0.69813170079</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <include>
      <uri>model://pybullet_ant/large_capsule</uri>
      <pose frame="torso_frame">-0.4 -0.4 0 0 0 3.14159265359</pose>
      <name>back_left_foot</name>
    </include>

    <joint name="ankle_3" type="revolute">
      <parent>aux_3::top</parent>
      <child>back_left_foot::bottom</child>
      <axis>
        <xyz>1 -1 0</xyz>
        <limit>
          <!-- -100 to -30 degrees-->
          <lower>-1.74532925199</lower>
          <upper>-0.52359877559</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <!-- < Back right part > -->

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">0 0 0 0 0 -1.57079632679</pose>
      <name>back_right_leg</name>
    </include>

    <joint name="torso_leg_joint_4" type="fixed">
      <parent>torso</parent>
      <child>back_right_leg::bottom</child>
    </joint>

    <include>
      <uri>model://pybullet_ant/small_capsule</uri>
      <pose frame="torso_frame">0.2 -0.2 0 0 0 -1.57079632679</pose>
      <name>aux_4</name>
    </include>

    <joint name="hip_4" type="revolute">
      <parent>back_right_leg::top</parent>
      <child>aux_4::bottom</child>
      <axis>
        <xyz>0 0 1</xyz>
        <limit>
          <!-- -40 to 40 degrees-->
          <lower>-0.69813170079</lower>
          <upper>0.69813170079</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <include>
      <uri>model://pybullet_ant/large_capsule</uri>
      <pose frame="torso_frame">0.4 -0.4 0 0 0 -1.57079632679</pose>
      <name>back_right_foot</name>
    </include>

    <joint name="ankle_4" type="revolute">
      <parent>aux_4::top</parent>
      <child>back_right_foot::bottom</child>
      <axis>
        <xyz>-1 1 0</xyz>
        <limit>
          <!-- 30 to 100 degrees-->
          <lower>0.52359877559</lower>
          <upper>1.74532925199</upper>
        </limit>
        <dynamics>
          <damping>1.0</damping>
        </dynamics>
      </axis>
    </joint>

    <plugin name="pybullet_ant_controller" filename="libpybullet_ant_plugin.so">
    </plugin>
  </model>
</sdf>